package com.maniavision;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class RectangleTest {

	Rectangle rectangle;
	
	@BeforeEach
	void setUp() {
		rectangle = new Rectangle(12.50, 5.12);
	}
	
	@Test
	@DisplayName("Calculate perimeter")
	void testPerimeterCalculation() {
		assertEquals(35.24, rectangle.getPerimeter(), "Perimeter should have been 35.24");
	}
	
	@Test
	@DisplayName("Calculate Area")
	void testAreaCalculation() {
		assertEquals(64, rectangle.getArea(), "Area should have been 64");
	}
}
