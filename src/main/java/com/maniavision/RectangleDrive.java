package com.maniavision;

import java.util.Scanner;

public class RectangleDrive {

	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		
		System.out.print("Please enter the width: ");
		double width = scanner.nextDouble();
		System.out.print("Please enter the length: ");
		double length = scanner.nextDouble();
		
		Rectangle rectangle = new Rectangle(width, length);
		
		System.out.println();
		System.out.println("******Your Rectangle Info******");
		System.out.println("W:\t" + width + "\nL:\t" + length
				         + "\nP:\t" +  rectangle.getPerimeter()
				         + "\nA:\t" + rectangle.getArea()
				         + "\nD:\t" + rectangle.getDiagonal());
		System.out.println("*******************************");
		

	}

}
