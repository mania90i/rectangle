package com.maniavision;

import java.text.DecimalFormat;

public class Rectangle {
	
	private double breadth;
	private double length;
	
	public Rectangle(double w, double l) {
		this.breadth = w;
		this.length = l;
	}

	public double getWidth() {
		return breadth;
	}

	public void setWidth(double width) {
		this.breadth = width;
	}

	public double getLength() {
		return length;
	}

	public void setLength(double length) {
		this.length = length;
	}
	
	public double getPerimeter() {
		return 2 * (this.breadth + this.length);
	}
	
	public double getArea() {
		return this.breadth * this.length;
	}
	
	public double getDiagonal() {
		double length_pow_2 = Math.pow(this.length, 2);
		double breadth_pow_2 = Math.pow(this.breadth, 2);
		double res = Math.sqrt(length_pow_2 + breadth_pow_2);
		DecimalFormat df = new DecimalFormat("##.00");
		return Double.valueOf(df.format(res));
	}

	@Override
	public String toString() {
		return "Rectangle [width=" + breadth + ", length=" + length + "]";
	}
	
	
}
